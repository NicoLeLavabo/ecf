<?php

namespace App\Controller;

use DateTime;
use App\Entity\Ticket;
use App\Form\ResponseType;
use App\Form\TicketFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="tickets")
     * @IsGranted("ROLE_USER")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Ticket::class);
        $tickets = $repo->findBy(['user' => $this->getUser()]);
        return $this->render('ticket/index.html.twig', [
                'tickets' => $tickets
        ]);
    }

     /**
     * @Route("/ticket/new", name="ticket_new")
     * @Route("/ticket/{id}/edit", name="ticket_edit")
     * @IsGranted("ROLE_USER")
     */
    public function formTicket(Ticket $ticket = null, Request $request, EntityManagerInterface $manager): Response
    {

        if (!$ticket) {
            $ticket = new Ticket();
        }

        $formTicket = $this->createForm(TicketFormType::class, $ticket);
        $formTicket->handleRequest($request);

        if ($formTicket->isSubmitted() && $formTicket->isValid()) {
            if (!$ticket->getId()) {
                $ticket->setDate(new DateTime());
                $ticket->setUser($this->getUser());
                $ticket->setStatus('ouvert');
            }
            $manager->persist($ticket);
            $manager->flush();

            return $this->redirectToRoute('ticket_show', ['id' => $ticket->getId()]);
        }

        return $this->render('ticket/create.html.twig', [
            'formTicket' => $formTicket->createView(),
            'editMode' => $ticket->getId() != null,
        ]);
    }
    /** 
    * @Route("/ticket/{id}", name="ticket_show")
    * @IsGranted("ROLE_USER")
    */
   public function show($id, EntityManagerInterface $manager, Request $request): Response
   {

       $repo = $this->getDoctrine()->getRepository(Ticket::class);
       $ticket = $repo->find($id);

       $response =  new \App\Entity\Response();
       $formResponse = $this->createForm(ResponseType::class, $response);
       $formResponse->handleRequest($request);

       if ($formResponse->isSubmitted() && $formResponse->isValid()) {
            $response->setDate(new DateTime());
            $response->setUser($this->getUser());
            $response->setTicket($ticket);
            $manager->persist($response);
            $manager->flush();

        return $this->redirectToRoute('ticket_show', ['id' => $ticket->getId()]);
    }

       return $this->render('ticket/show.html.twig', [
           'ticket' => $ticket,
           'formResponse' => $formResponse->createView(),
       ]);
   }

   /**
     * @Route("/support/ticket", name="support_tickets")
     * @IsGranted("ROLE_SUPPORT")
     */
    public function supportTickets(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Ticket::class);
        $tickets = $repo->findAll();
        return $this->render('ticket/index.html.twig', [
                'tickets' => $tickets
        ]);
    }
}
