<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\PassType;
use App\Form\EmailType;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/profil/", name="profil")
     */
    public function profil_informations(): Response
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user = $repo->find($this->getUser());
        return $this->render('user/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/profil/update/email/", name="email_update")
     */
    public function update_email(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(EmailType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('profil');
        }

        return $this->render('user/update-email.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profil/update/password/", name="password_update")
     */
    public function update_password(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $hash = $this->getUser()->getPassword();

        $form = $this->createForm(PassType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            if (password_verify($password, $hash)) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('newPassword')->getData()
                    )
                );
                $this->addFlash('success', 'Mot de passe modifié avec succès !');
                $manager->persist($user);
                $manager->flush();
                return $this->redirectToRoute('profil');
            } else {
                $form->get('password')->addError(new FormError('Mauvais mot de passe'));
            }
        }
        return $this->render('user/update-password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}