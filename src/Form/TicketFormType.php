<?php

namespace App\Form;

use App\Entity\Ticket;
use App\Entity\Category;
use Doctrine\ORM\Query\Expr\Select;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TicketFormType extends AbstractType
{
    /**
    * @var AuthorizationChecker
    */
    private $authorizationChecker=null;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
      $this->authorizationChecker=$authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message')
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'name',
            ));
            
            if($this->authorizationChecker->isGranted('ROLE_SUPPORT') || $this->authorizationChecker->isGranted('ROLE_ADMIN'))
            {
              $builder->add('status', ChoiceType::class, [
                'choices'  => [
                    'Ouvert' => 'ouvert',
                    'Fermé' => 'ferme',
                ],
            ]);
            }
            $builder->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
